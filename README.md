# Linkage Lugares

Through this API, resources related to the places of the Linkage platform are stored


### GET /lugares

**Request**

- `GET /lugares`

**Response**

- `200 OK` on succes
```json
[
    {
        "id_lugar": "34212",
        "nombre": "Las Delicias",
        "descripción": "Un lugar para eventos sociales como graduaciones y matrimonios",
        "id_categoria": 13,
        "id_ciudad": 564,
        "direccion": "Km 3 via Rionegro-Aeropuerto",
        "id_administrador": 423
    },
    {
        "id_lugar": "4234",
        "nombre": "El Chato",
        "descripción": "Venta de comidas rápidas para disfrutar con amigos y familiares",
        "id_categoria": 10,
        "id_ciudad": 564,
        "direccion": "Carrera 52 #45-32",
        "id_administrador": 245
    },
    {
    
    }
]
```

- `404 NOT FOUND` on empty database
```json
[ ]
```


### POST /lugares

**Request**

- `POST /lugares`

```json
[
    {
        "id_lugar": "34212",
        "nombre": "Las Delicias",
        "descripción": "Un lugar para eventos sociales como graduaciones y matrimonios",
        "id_categoria": 13,
        "id_ciudad": 564,
        "direccion": "Km 3 via Rionegro-Aeropuerto",
        "id_administrador": 423
    },
    {
        "id_lugar": "4234",
        "nombre": "El Chato",
        "descripción": "Venta de comidas rápidas para disfrutar con amigos y familiares",
        "id_categoria": 10,
        "id_ciudad": 564,
        "direccion": "Carrera 52 #45-32",
        "id_administrador": 245
    },
    {
    }
]
```

**Response**

- `201 OK` on success (all resources were created)

- `409 CONFLICT` some resources were created before


### GET /lugar/<id_lugar>

**Request**

- `GET /lugar/<id_lugar>`

**Response**

- `200 OK` resource found

```json
{
    "id_lugar": "34212",
    "nombre": "Las Delicias",
    "descripción": "Un lugar para eventos sociales como graduaciones y matrimonios",
    "id_categoria": 13,
    "id_ciudad": 564,
    "direccion": "Km 3 via Rionegro-Aeropuerto",
    "id_administrador": 423
}
```

- `404 NOT FOUND` resource not found


### POST /lugar

**Request**

- `POST /lugar`
```json
{
    "id_lugar": "34212",
    "nombre": "Las Delicias",
    "descripción": "Un lugar para eventos sociales como graduaciones y matrimonios",
    "id_categoria": 13,
    "id_ciudad": 564,
    "direccion": "Km 3 via Rionegro-Aeropuerto",
    "id_administrador": 423
}
```

**Response**

- `201 OK` on success (resource created)
- `409 Conflict` resource has been created before


### PUT /lugar

**Request**

- `PUT /lugar`
```json
{
    "id_lugar": "34212",
    "nombre": "Las Delicias",
    "descripción": "Un lugar para eventos sociales como graduaciones y matrimonios",
    "id_categoria": 13,
    "id_ciudad": 564,
    "direccion": "Km 3 via Rionegro-Aeropuerto",
    "id_administrador": 423
}
```

**Response**

- `201 OK` on success (resource updated)
- `404 NOT FOUND` Resource not found to be updated


### DELETE /lugar/<id_lugar>

request:
- `DELETE /lugar/<id_lugar>`

response:
 - `201 OK` on success (resource deleted)
 - `404 NOT FOUND` Resource not found to be deleted

