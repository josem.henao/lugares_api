# The Database Engine. use something like this: 'mysql', 'oracle',
# 'postgresql', etc.
db_engine = "mysql"

# The database's variables for connection
db_config = {
    "db_url": 'localhost',
    "db_port": '3306',
    "db_name": 'lkg_lugares',
    "db_user": 'root',
    "db_password": 'admin123*'
}

# The stable version of API
api_version = 'v1'

# The API's state of development
snapshot = '1.2'
