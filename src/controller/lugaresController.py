from flask import jsonify, request, render_template

from config import api_version as version, snapshot, db_engine
from serializer.lugarSerializer import LugarSerializer
from services.lugarService import LugarService


def lugaresController(app):
    lugarService = LugarService()

    @app.route(f'/{version}')
    @app.route(f'/{version}/home')
    def home_GET():
        return render_template("index.html", version=version,
                               snapshot=snapshot, db_engine=db_engine
                               )

    @app.route(f'/{version}/lugares', methods=['GET'])
    def get_lugares():
        return jsonify(lugarService.list_all())

    @app.route(f'/{version}/lugares', methods=['POST'])
    def post_lugares():
        print(request.get_json())
        lugarSerializer = LugarSerializer(**request.get_json())
        model = lugarSerializer.get_as_model()
        print(model)
        resultado = lugarService.save(model)
        print("resultado", resultado)
        return "saved"

    @app.route(f'/{version}/lugar/<int:id_lugar>', methods=['GET'])
    def get_lugar(*args, **kwargs):
        return jsonify({
            'mensaje': 'GET lugar/{}'.format(kwargs.get('id_lugar'))
        })

    @app.route(f'/{version}/lugar', methods=['POST'])
    def post_lugar(*args, **kwargs):
        return jsonify({
            'mensaje': 'POST /lugar'
        })

    @app.route(f'/{version}/lugar', methods=['PUT'])
    def lugar_put(*args, **kwargs):
        return jsonify({
            'mensaje': 'PUT /lugar'
        })

    @app.route(f'/{version}/lugares/<int:id_lugar>', methods=['DELETE'])
    def lugar_delete(*args, **kwargs):
        return jsonify({
            'mensaje': 'DELETE lugar/{}'.format(kwargs.get('id_lugar'))
        })
