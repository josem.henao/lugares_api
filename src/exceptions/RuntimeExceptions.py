from exceptions.genericExceptions import ApiException


class ResourceCreatedBefore(ApiException):
    def __init__(self, message, description, status):
        super().__init__(code=2000, error="ResourceCreatedBefore", message=message,
                         description=description, status=status
                         )
