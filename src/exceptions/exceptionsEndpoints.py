from flask import jsonify

from exceptions.validationExceptions import NoParametersError
from exceptions.genericExceptions import ApiException


def lugaresExceptions(app):
    """
    Handle all Api Exception generated during execution of the same
    :param app:
    :return: response
    """
    @app.errorhandler(ApiException)
    def handle_api_error(ex):
        response = jsonify(ex.code, ex.error, ex.message, ex.description)
        response.status_code = ex.status
        return response

    @app.errorhandler(NoParametersError)
    def handle_NoParamatersError(ex):
        response = jsonify(ex.code, ex.error, ex.message, ex.description)
        response.status_code = ex.status
        return response
