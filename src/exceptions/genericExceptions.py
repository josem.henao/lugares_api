class ApiException(Exception):
    """This is the parent exception in the whole API.

        :param code: The code of the specific Exception that inherits
                     ApiException

        :param error: The tecnichal name of the error.

        :param message: A short message understandable by humans.

        :param description: A technical message understandable by developers.

        :param status: An http code that describes in the best way the status
                       of the request made by the client.e.g. CONFLICT (409),
                       there is a problem with the data that has been sent by
                       the client and the client must resubmit the request.

        """

    def __init__(self, code: int, error: str, message: str, description: str,
                 status: int):
        self.code = code
        self.error = error
        self.message = message
        self.description = description
        self.status = status

    def __str__(self):
        return "Exception {}: {}".format(self.code, self.description)