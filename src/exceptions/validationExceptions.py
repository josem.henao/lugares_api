from exceptions.genericExceptions import ApiException


class InvalidType(ApiException):
    def __init__(self, message, description, status):
        super().__init__(code=1001, error="InvalidType", message=message,
                         description=description, status=status
                         )


class NotIntegerError(ApiException):
    def __init__(self, message, description, status):
        super().__init__(code=1002, error="NotIntegerError", message=message,
                         description=description, status=status
                         )


class NotStringError(ApiException):
    def __init__(self, message, description, status):
        super().__init__(code=1003, error="NotStringError", message=message,
                         description=description, status=status
                         )


class PropertyRequired(ApiException):
    def __init__(self, message, description, status):
        super().__init__(code=1004, error="PropertyRequired", message=message,
                         description=description, status=status
                         )


class LengthMinor(ApiException):
    def __init__(self, message, description, status):
        super().__init__(code=1005, error="LengthMinor", message=message,
                         description=description, status=status
                         )


class NoParametersError(ApiException):
    def __init__(self, message, description, status):
        super().__init__(code=1006, error="LengthMinor", message=message,
                         description=description, status=status
                         )
