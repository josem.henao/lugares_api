from repository.lugarRepository import LugarRepository
from util.wrappers import capitalize


class Lugar():
    def __init__(self, **kwargs):
        for k in kwargs:
            """
                It is necessarly to clean `_` caracter if k comes with it
            """
            k_i = k[1:] if k[0] == '_' else k
            self.__setattr__(k_i, kwargs.get(k))

    @property
    def id_lugar(self):
        return self._id_lugar

    @id_lugar.setter
    def id_lugar(self, id_lugar):
        self._id_lugar = id_lugar

    @property
    def nombre(self):
        return self._nombre

    @nombre.setter
    @capitalize
    def nombre(self, nombre):
        self._nombre = nombre

    @property
    def descripcion(self):
        return self._descripcion

    @descripcion.setter
    def descripcion(self, descripcion):
        self._descripcion = descripcion

    @property
    def id_categoria(self):
        return self._id_categoria

    @id_categoria.setter
    def id_categoria(self, id_categoria):
        self._id_categoria = id_categoria

    @property
    def id_ciudad(self):
        return self._id_ciudad

    @id_ciudad.setter
    def id_ciudad(self, id_ciudad):
        self._id_ciudad = id_ciudad

    @property
    def id_administrador(self):
        return self._id_administrador

    @id_administrador.setter
    def id_administrador(self, id_administrador):
        self._id_administrador = id_administrador

    @property
    def direccion(self):
        return self._direccion

    @direccion.setter
    def direccion(self, direccion):
        self._direccion = direccion

    def get_as_repository(self):
        lugarRespository = LugarRepository(
            nombre=self.nombre,
            descripcion=self.descripcion,
            id_categoria=self.id_categoria,
            id_ciudad=self.id_ciudad,
            id_administrador=self.id_administrador,
            direccion=self.direccion
        )

        return lugarRespository

    def __str__(self):
        return self.nombre

# las_delicias = Lugar(nombre="Las Delicias", id_administrador=232)
# print(las_delicias)
# print(las_delicias.id_administrador)
# print(las_delicias.__dict__)
