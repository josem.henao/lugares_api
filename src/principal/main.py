from controller.lugaresController import lugaresController
from exceptions.exceptionsEndpoints import lugaresExceptions
from principal.app import app, run_app


def main():

    # Mapping endpoints
    lugaresController(app)

    # Mapping endpoints for exceptions
    lugaresExceptions(app)

    # Running App
    run_app()

if __name__ == '__main__':
    main()
