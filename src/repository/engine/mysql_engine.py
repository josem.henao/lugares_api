from sqlalchemy import create_engine


class MysqlEngine():
    _engine = None
    def __init__(self, db_config):
        _db_url = db_config['db_url']
        _db_port = db_config['db_port']
        _db_name = db_config['db_name']
        _db_user = db_config['db_user']
        _db_password = db_config['db_password']
        self._engine = create_engine(
            f'mysql+pymysql://{_db_user}:{_db_password}@{_db_url}:{_db_port}/{_db_name}'
        )

    def get_engine(self):
        return self._engine