from sqlalchemy.orm import sessionmaker
from repository.engine.mysql_engine import MysqlEngine
from config import db_config, db_engine
from repository.base import Base


class Session():
    def __init__(self):
        _Session = None
        if db_engine == "mysql":
            mysqlEngine = MysqlEngine(db_config).get_engine()

            #  Creating Database
            Base.metadata.create_all(mysqlEngine)

            # Defining Session
            self._Session = sessionmaker(bind=mysqlEngine)()
        else:
            raise Exception("No se ha especificado un motor de BD permitido en"
                            "el archivo de configuraciones de la API")

    def get_session(self):
        return self._Session

