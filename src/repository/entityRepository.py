from sqlalchemy import Column, Integer, DateTime


class EntityRepository():
    id_lugar = Column(Integer, primary_key=True, autoincrement=True)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    def __init__(self, id_lugar=None):
        if id_lugar:
            self.id_lugar = id_lugar
