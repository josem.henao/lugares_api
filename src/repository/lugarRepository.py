from datetime import datetime

from sqlalchemy import Column, String, Integer

from repository.base import Base
from repository.entityRepository import EntityRepository


class LugarRepository(EntityRepository, Base):
    __tablename__ = "lugares"

    nombre = Column(String(50))
    descripcion = Column(String(256))
    id_categoria = Column(Integer)
    id_ciudad = Column(Integer)
    direccion = Column(String(120))
    id_administrador = Column(Integer)

    def __init__(self, nombre, descripcion, id_categoria, id_ciudad, direccion, id_administrador, id_lugar=None):
        if id_lugar:
            super(id_lugar)
        self.nombre = nombre
        self.descripcion = descripcion
        self.id_categoria = id_categoria
        self.id_ciudad = id_ciudad
        self.direccion = direccion
        self.id_administrador = id_administrador
        self.created_at = datetime.now()
        self.updated_at = datetime.now()

    def get_as_dict(self):
        return self.__dict__
