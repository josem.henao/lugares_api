from exceptions.validationExceptions import NoParametersError
from models.lugar import Lugar
from serializer.serializer import Serializer
from util.wrappers import is_integer, is_string, is_required, \
     min_length, max_length
from util.httpCodes import CONFLICT


class LugarSerializer(Serializer):

    def __init__(self, **kwargs):
        if len(kwargs) == 0:
            raise NoParametersError(
                "No se han pasado datos para crear un Lugar",
                "No se han pasado parámetros en el constructor del Lugar",
                CONFLICT
            )
        else:
            for k in kwargs.keys():
                self.__setattr__(k, kwargs[k])

    @property
    def id_lugar(self):
        return self._id_lugar

    @id_lugar.setter
    @is_integer
    def id_lugar(self, id_lugar):
        self._id_lugar = id_lugar

    @property
    def nombre(self):
        return self._nombre

    @nombre.setter
    @is_string
    @is_required
    @min_length(2)
    def nombre(self, nombre):
        self._nombre = nombre

    @property
    def descripcion(self):
        return self._descripcion

    @descripcion.setter
    @is_string
    @max_length(255)
    def descripcion(self, descripcion):
        self._descripcion = descripcion

    @property
    def id_categoria(self):
        return self._id_categoria

    @id_categoria.setter
    @is_integer
    @is_required
    def id_categoria(self, id_categoria):
        self._id_categoria = id_categoria

    @property
    def id_ciudad(self):
        return self._id_ciudad

    @id_ciudad.setter
    @is_integer
    @is_required
    def id_ciudad(self, id_ciudad):
        self._id_ciudad = id_ciudad

    @property
    def id_administrador(self):
        return self._id_administrador

    @id_administrador.setter
    @is_integer
    def id_administrador(self, id_administrador):
        self._id_administrador = id_administrador

    @property
    def direccion(self):
        return self._descripcion

    @direccion.setter
    @is_string
    @max_length(255)
    def direccion(self, direccion):
        self._direccion = direccion

    def get_as_model(self):
        return Lugar(**self.get_as_dict())

    def get_as_dict(self):
        return self.__dict__

    def __str__(self):
        return self.nombre

# lugar = LugarSerializer(nombre="Las Delicias",
#                         descripcion="Descripción de las delicias",
#                         id_categoria=1002)

# lugar = LugarSerializer(nombre="las Delicias",
#                         descripcion="Descripcion de las Delicias",
#                         id_categoria=1004, id_ciudad=605,
#                         id_administrador=1067,
#                         direccion="Km 3 via Rionegro- Aeropuerto"
#                         )

# lugar = LugarSerializer()
#
# print(lugar.get_as_dict())
# print(lugar.id_lugar)
# print(lugar.get_as_model().nombre)
# print(lugar.nombre)

# **{'nombre': 'Jose', 'edad': 25, 'ciudad': 'Rionegro'}
