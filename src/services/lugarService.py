from exceptions.RuntimeExceptions import ResourceCreatedBefore
from models.lugar import Lugar
from repository.engine.session import Session
from repository.lugarRepository import LugarRepository


class LugarService():
    _lugarSchema = None
    _session = None

    def __init__(self):
        self._session = Session().get_session()

    def find_all(self):
        result = self._session.query(LugarRepository).all()
        return self._clean_result(result)

    def find_by_id_lugar(self, id_lugar):
        result = self._session.query(LugarRepository).filter_by(
            id_lugars=id_lugar).all()
        return self._clean_result(result)

    def find_by_nombre(self, nombre):
        result = self._session.query(LugarRepository).filter_by(
            nombre=nombre).all()
        return self._clean_result(result)

    def find_by_id_ciudad(self, id_ciudad):
        result = self._session.query(LugarRepository).filter_by(
            id_ciudad=id_ciudad).all()
        return self._clean_result(result)

    def find_by_id_categoria(self, id_categoria):
        result = self._session.query(LugarRepository).filter_by(
            id_categoria=id_categoria).all()
        return self._clean_result(result)

    def save(self, lugar):

        lugares_finded = self.find_by_nombre(lugar.nombre)
        if lugares_finded:
            print("exists")
            raise ResourceCreatedBefore("El recurso que se quiere crear y")
        else:
            saved = self._session.add(lugar.get_as_repository())
            self._session.commit()
            return True

    def _clean_result(self, result):
        response = []
        for e in result:
            e_d = e.get_as_dict()
            e_d.pop("_sa_instance_state")
            response.append(e_d)
        return response

    def __str__(self):
        pass

    def __enter__(self):
        #abre la session
        pass

    def __exit__(self, exc_type, exc_val, exc_tb):
        #cierra la sesion



    def _lugar_exists(self, lugar):
        """
        procedimiento para verificar que un recurso de Lugar con el mismo nombre
         y en la misma ciudad ya existe en la base de datos
        :param lugar: intancia de la clase Lugar
        :return: True -> en caso de que se valide que existe el lugar en la
                         base de datos
                 False -> en caso de que no exista el recurso en la base de
                          datos
        """


# lugarService = LugarService()
# result = lugarService.find_by_id_categoria(458)
#
# for e in result:
#     print(f"---- {e['id_lugar']} ----")
#     print(e)

lugarService = LugarService()
lugar = Lugar(nombre="LAS delicias")
print(lugar)
print(lugarService.save(lugar))
print()


