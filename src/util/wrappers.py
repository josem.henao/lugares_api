from exceptions.validationExceptions import NotIntegerError, NotStringError, \
    PropertyRequired, LengthMinor
from util.httpCodes import CONFLICT


def is_integer(f) -> object:
    def wrapper(*args):
        s = args[0] if len(args) > 1 else None
        value = args[1] if len(args) > 1 else args[0]
        if type(value) is not int:
            raise NotIntegerError("Se ha generado un error en la "
                                  "serialización de recursos",
                                  "El tipo de dato no es entero como se espera",
                                  CONFLICT
                                  )
        else:
            return f(s, value) if len(args) > 1 else f(value)

    return wrapper


def is_string(f) -> object:
    def wrapper(*args):
        s = args[0] if len(args) > 1 else None
        value = args[1] if len(args) > 1 else args[0]
        if type(value) is not str:
            raise NotStringError("Se ha generado un error en la "
                                 "serialización de recursos",
                                 "El tipo de dato no es String como se espera",
                                 CONFLICT
                                 )
        else:
            return f(s, value) if len(args) > 1 else f(value)

    return wrapper


def is_required(f) -> object:
    def wrapper(*args):
        s = args[0] if len(args) > 1 else None
        value = args[1] if len(args) > 1 else args[0]
        if not value:
            raise PropertyRequired("Se ha generado un error en la "
                                   "serialización de recursos",
                                   "No se ha dado un dato para un campo requerido",
                                   CONFLICT
                                   )
        else:
            return f(s, value) if len(args) > 1 else f(value)

    return wrapper


def capitalize(f) -> object:
    def wrapper(*args):
        s = args[0] if len(args) > 1 else None
        cadena = args[1] if len(args) > 1 else args[0]
        cadena = cadena.title()
        return f(s, cadena) if len(args) > 1 else f(cadena)

    return wrapper


def min_length(*args, **options) -> object:
    min_len = options.get('min_len') if options else None
    min_len = args[0] if not min_len else min_len

    def wrapper(*args):
        f = args[0]

        def decorator(*args):
            s = args[0] if len(args) > 1 else None
            value = args[1] if len(args) > 1 else args[0]
            if len(str(value)) < min_len:
                raise LengthMinor("Se ha generado un error en la "
                                  "serialización de recursos",
                                  "la longitud de uno de los datos no tiene "
                                  "la longitud esperada ({} < {})".format(
                                      len(str(value)), min_len
                                  ),
                                  CONFLICT
                                  )
            else:
                return f(s, value) if len(args) > 1 else f(value)

        return decorator

    return wrapper


def max_length(*args, **options) -> object:
    max_len = options.get('max_len') if options else None
    max_len = args[0] if not max_len else max_len

    def wrapper(*args):
        f = args[0]

        def decorator(*args):
            s = args[0] if len(args) > 1 else None
            value = args[1] if len(args) > 1 else args[0]
            if len(str(value)) > max_len:
                raise LengthMinor("Se ha generado un error en la "
                                  "serialización de recursos",
                                  "la longitud de uno de los datos es mayor a "
                                  "la longitud esperada ({} > {})".format(
                                      len(str(value)), max_len
                                  ),
                                  CONFLICT
                                  )
            else:
                return f(s, value) if len(args) > 1 else f(value)

        return decorator

    return wrapper

# @is_integer
# @is_required
# @min_length(3)
# @max_length(10)
# def set_value(value):
#     print("The value inside the function is:", value)
#
# set_value(12345678901)
